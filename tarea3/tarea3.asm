	org 	100h

section .text

    call    col
	call 	texto	
	call 	cursor
	call 	phrase
    call 	phrase1
    call 	phrase2
    call	kbwait
	int 	20h

col:  mov     al, 5d
        mov     [202h], al
        ret

texto:	mov 	ah, 00h
	mov	al, 03h
	int 	10h
	ret

cursor: mov	ah, 01h
	mov 	ch, 00000000b
	mov 	cl, 00001110b
		;   IRGB
	int 	10h
	ret

w_char:	mov 	ah, 09h
	mov 	al, cl
	mov 	bh, 0h
	mov 	bl, 00001111b
	mov 	cx, 1h
	int 	10h
	ret

kbwait: mov 	ax, 0000h
	int 	16h
	ret

m_cursr:mov 	ah, 02h
	mov 	dx, di  ; columna
	mov 	dh, [202h] ; fila
	mov 	bh, 0h
	int 	10h
	ret

row:mov     [200h], ax
        mov     ax, 4h
        add     [202h], ax
        mov     ax, [200h]
        ret

phrase:	mov 	di, 5d
lupi:	mov 	cl, [msg+di-5d]
	call    m_cursr
	call 	w_char
	inc	di
	cmp 	di, len
	jb	lupi
    call row
	ret

phrase1:	mov 	di, 15d
lupi1:	mov 	cl, [msg1+di-15d]
	call    m_cursr
	call 	w_char
	inc	di
	cmp 	di, len1
	jb	lupi1
    call row
	ret

phrase2:	mov 	di, 25d
lupi2:	mov 	cl, [msg2+di-25d]
	call    m_cursr
	call 	w_char
	inc	di
	cmp 	di, len2
	jb	lupi2
    call row
	ret

section .data
msg	    db 	"a@@@@@@@@a  a@@@@@@a  a@@@@@@@a"
len 	equ	$-msg+5d
msg1	db 	"@@@@  @@@@ @@@@  @@@@ @@@@  @@@"
len1 	equ	$-msg1+15d
msg2	db 	"@@@@  @@@@ @@@@  @@@@ @@@@  @@@"
len2 	equ	$-msg2+25d

