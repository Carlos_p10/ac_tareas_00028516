org 	100h

section .text

	mov 	dx, msg
	call 	w_strng

	xor 	si, si 	;lo mimso que: mov si, 0000h
	xor 	cl, cl

lupi:	call 	kb	
	cmp 	al, "$"
	je	clean
	sub 	al,30h
	mov	[300h+si], al ; CS:0300h en adelante
	inc 	si
	jmp 	lupi

clean: xor bx,bx
	   xor ax, ax
prom:   add 	al, [300h+bx]
		inc  	bx
		cmp 	bx,5d
		jb 		prom	
		mov 	[310h], al
		mov 	[320h], bl
		mov 	cl,bl
		div 	cl
		mov 	[330h], al

mostrar:mov 	dx, nl
	call 	w_strng
	cmp 	al, 1d
	je 		txt1
	cmp 	al, 2d
	je 		txt2
	cmp 	al, 3d
	je 		txt3
	cmp 	al, 4d
	je 		txt4
	cmp 	al, 5d
	je 		txt5
	cmp 	al, 6d
	je 		txt6
	cmp 	al, 7d
	je 		txt7
	cmp 	al, 8d
	je 		txt8
	cmp 	al, 9d
	je 		txt9
	cmp 	al, 10d
	je 		txt10
	; call	w_strng
	; mov	byte [200h+si], "$"
	; mov 	dx, 200h
	; call 	w_strng

	call 	kb	; solo detenemos la ejecución
	int 	20h

txt1: mov 	dx, m1
	jmp 	stri

txt2: mov 	dx, m2
	jmp 	stri

txt3: mov 	dx, m3
	jmp 	stri

txt4: mov 	dx, m4
	jmp 	stri

txt5: mov 	dx, m5
	jmp 	stri

txt6: mov 	dx, m6
	jmp 	stri

txt7: mov 	dx, m7
	jmp 	stri

txt8: mov 	dx, m8
	jmp 	stri

txt9: mov 	dx, m9
	jmp 	stri

txt10: mov 	dx, m10
	jmp 	stri

stri: call w_strng
	  call kb
	  int 20h

texto:	mov 	ah, 00h
	mov	al, 03h
	int 	10h
	ret

kb: 	mov	ah, 1h
	int 	21h
	ret

w_strng:mov	ah, 09h
	int 	21h
	ret

section .data

msg 	db 	"Ingrese ultimos 5 digitos del carne: $"
m1 	db 	"Solo necesito el 0$"
m2 	db 	"Aun se pasa$"
m3 	db 	"Hay salud $"
m4 	db 	"Me recupero$"
m5 	db 	"En el segundo$"
m6 	db 	"Peor es nada$"
m7 	db 	"Muy bien$"
m8 	db 	"Colocho$"
m9 	db 	"Siempre me esfuerzo$"
m10 	db 	"Perfecto solo es Dios$" 
nl	db 	0xA, 0xD, "$"
; --Codigo Fuente -> Instructor