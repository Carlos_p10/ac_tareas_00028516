org 	100h

section .text

	call 	grafico	; Llamada a iniciar modo grafico 13h

	xor 	si, si
	xor 	di, di

	mov 	si, 50d ; X -> Columna
	mov 	di, 50d ; Y -> Fila
	call 	linea_h

    mov     si, 100d
    mov     di, 35d
    call    linea_v2

    mov 	si, 35d ; X -> Columna
	mov 	di, 35d ; Y -> Fila
	call 	linea_h

	mov 	si, 50d ; X -> Columna
	mov 	di, 100d ; Y -> Fila
	call 	linea_h

    mov 	si, 100d ; X -> Columna
	mov 	di, 100d ; Y -> Fila
	call 	linea_v3

    mov 	si, 35d ; X -> Columna
	mov 	di, 115d ; Y -> Fila
	call 	linea_h

	mov	si, 50d ; X -> Columna
	mov 	di, 50d ; Y -> Fila
	call 	linea_v

	mov	si, 35d ; X -> Columna
	mov 	di, 35d ; Y -> Fila
	call 	linea_v3

	call 	kb		; Utilizamos espera de alguna tecla

	int 	20h

grafico:mov	ah, 00h
	mov	al, 13h
	int 	10h
	ret

pixel:	mov	ah, 0Ch
	mov	al, 1010b
	int 	10h
	ret

linea_h: 
lupi_h:	mov 	cx, 0d ; Columna 
	add 	cx, si
	mov	dx, di ; Fila
	call 	pixel
	inc 	si
	cmp 	si, 100d
	jne 	lupi_h
	ret

linea_v:
lupi_v:	mov 	cx, si ; Columna 
	mov	dx, 0d ; Fila
	add 	dx, di
	call 	pixel
	inc 	di
	cmp 	di, 100d
	jne 	lupi_v
	ret

linea_v2:
lupi_v2:	mov 	cx, si ; Columna 
	mov	dx, 0d ; Fila
	add 	dx, di
	call 	pixel
	inc 	di
	cmp 	di, 50d
	jne 	lupi_v2
	ret

linea_v3:
lupi_v3:	mov 	cx, si ; Columna 
	mov	dx, 0d ; Fila
	add 	dx, di
	call 	pixel
	inc 	di
	cmp 	di, 115d
	jne 	lupi_v3
	ret

kb: 	mov	ah, 00h
	int 	16h
	ret

section .data

;Codigo base de Labo 10 by Nelson Castro